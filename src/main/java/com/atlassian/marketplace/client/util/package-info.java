/**
 * Miscellaneous helper classes and methods.
 * @since 2.0.0
 */
package com.atlassian.marketplace.client.util;